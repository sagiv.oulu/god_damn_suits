# God Damn suits
The TV series [Suits](https://en.wikipedia.org/wiki/Suits_(American_TV_series)) likes to use the fraze "God Damn" a lot.
So I started wondering: How many God damns do they say?
That is why I wrote this little script do do that.

## How does it work
The script pulls every episode & its script from [this](https://www.springfieldspringfield.co.uk/episode_scripts.php?tv-show=suits) website, & counts the number of god damns.

## How to run
1) Install python3.6 or above
2) Install requirements by running:
```pip3 install -r requirements.txt```
3) Run the script with the command:
```python main.py```

## Output
```shell script
s01e01 - Pilot - 7 god damns
s01e02 - Errors and Omissions - 1 god damns
s01e03 - Inside Track - 5 god damns
s01e04 - Dirty Little Secrets - 0 god damns
s01e05 - Bail Out - 3 god damns
s01e06 - Tricks of the Trade - 2 god damns
s01e07 - Play The Man - 0 god damns
s01e08 - Identity Crisis - 0 god damns
s01e09 - Undefeated - 1 god damns
s01e10 - The Shelf Life - 0 god damns
s01e11 - Rules Of The Game - 0 god damns
s01e12 - Dog Fight - 2 god damns
s02e01 - She Knows - 4 god damns
s02e02 - The Choice - 1 god damns
s02e03 - Meet the New Boss - 2 god damns
s02e04 - Discovery - 2 god damns
s02e05 - Break Point - 7 god damns
s02e06 - All In - 4 god damns
s02e07 - Sucker Punch - 2 god damns
s02e08 - Rewind - 6 god damns
s02e09 - Asterisk - 1 god damns
s02e10 - High Noon - 1 god damns
s02e11 - Blind-Sided - 2 god damns
s02e12 - Blood in the Water - 2 god damns
s02e13 - Zane Vs. Zane - 0 god damns
s02e14 - He's Back - 4 god damns
s02e15 - Normandy - 0 god damns
s02e16 - War - 4 god damns
s03e01 - The Arrangement - 6 god damns
s03e02 - I Want You to Want Me - 0 god damns
s03e03 - Unfinished Business - 1 god damns
s03e04 - Conflict Of Interest - 3 god damns
s03e05 - Shadow Of A Doubt - 2 god damns
s03e06 - The Other Time - 6 god damns
s03e07 - She's Mine - 3 god damns
s03e08 - Endgame - 5 god damns
s03e09 - Bad Faith - 7 god damns
s03e10 - Stay - 0 god damns
s03e11 - Buried Secrets - 0 god damns
s03e12 - Yesterday's Gone - 3 god damns
s03e13 - Moot Point - 3 god damns
s03e14 - Heartburn - 2 god damns
s03e15 - Know When to Fold 'Em - 8 god damns
s03e16 - No Way Out - 6 god damns
s04e01 - One-Two-Three Go... - 2 god damns
s04e02 - Breakfast, Lunch and Dinner - 6 god damns
s04e03 - Two in the Knees - 6 god damns
s04e04 - Leveraged - 7 god damns
s04e05 - Pound of Flesh - 3 god damns
s04e06 - Litt the Hell Up - 16 god damns
s04e07 - We're Done - 5 god damns
s04e08 - Exposure - 3 god damns
s04e09 - Gone - 11 god damns
s04e10 - This is Rome - 4 god damns
s04e11 - Enough Is Enough - 3 god damns
s04e12 - Respect - 2 god damns
s04e13 - Fork in the Road - 6 god damns
s04e14 - Derailed - 8 god damns
s04e15 - Intent - 7 god damns
s04e16 - Not Just a Pretty Face - 4 god damns
s05e01 - Denial - 6 god damns
s05e02 - Compensation - 5 god damns
s05e03 - No Refills - 5 god damns
s05e04 - No Puedo Hacerlo - 2 god damns
s05e05 - Toe to Toe - 5 god damns
s05e06 - Privilege - 6 god damns
s05e07 - Hitting Home - 8 god damns
s05e08 - Mea Culpa - 6 god damns
s05e09 - Uninvited Guests - 8 god damns
s05e10 - Faith - 1 god damns
s05e11 - Blowback - 10 god damns
s05e12 - Live to Fight - 12 god damns
s05e13 - God's Green Earth - 17 god damns
s05e14 - Self Defense - 11 god damns
s05e15 - Tick Tock - 10 god damns
s05e16 - 25th Hour - 13 god damns
s06e01 - To Trouble - 10 god damns
s06e02 - Accounts Payable - 11 god damns
s06e03 - Back on the Map - 13 god damns
s06e04 - Turn - 9 god damns
s06e05 - Trust - 9 god damns
s06e06 - Spain - 8 god damns
s06e07 - Shake the Trees - 9 god damns
s06e08 - Borrowed Time - 15 god damns
s06e09 - The Hand That Feeds - 6 god damns
s06e10 - P.S.L. - 8 god damns
s06e11 - She's Gone - 5 god damns
s06e12 - The Painting - 2 god damns
s06e13 - Teeth, Nose, Teeth - 7 god damns
s06e14 - Admission of Guilt - 5 god damns
s06e15 - Quid Pro Quo - 3 god damns
s06e16 - Character and Fitness - 3 god damns
s07e01 - Skin in the Game - 10 god damns
s07e02 - The Statue - 8 god damns
s07e03 - Mudmare - 8 god damns
s07e04 - Divide and Conquer - 7 god damns
s07e05 - Brooklyn Housing - 5 god damns
s07e06 - Home to Roost - 10 god damns
s07e07 - Full Disclosure - 10 god damns
s07e08 - 100 - 8 god damns
s07e09 - Shame - 7 god damns
s07e10 - Donna - 6 god damns
s07e11 - Hard Truths - 1 god damns
s07e12 - Bad Man - 8 god damns
s07e13 - Inevitable - 0 god damns
s07e14 - Pulling the Goalie - 2 god damns
s07e15 - Tiny Violin - 7 god damns
s07e16 - Good-Bye - 10 god damns
s08e01 - Right-Hand Man - 4 god damns
s08e02 - Pecking Order - 5 god damns
s08e03 - Promises Promises - 9 god damns
s08e04 - Revenue per square foot - 11 god damns
s08e05 - Good Mudding - 4 god damns
s08e06 - Cats Ballet Harvey Specter - 2 god damns
s08e07 - Sour Grapes - 7 god damns
s08e08 - Coral Gables - 7 god damns
s08e09 - Motion to Delay - 1 god damns
s08e10 - Managing Partner - 5 god damns
s08e11 - Rocky 8 - 3 god damns
s08e12 - Whale Hunt - 4 god damns
s08e13 - The Greater Good - 1 god damns
s08e14 - Peas in a Pod - 3 god damns
s08e15 - Stalking Horse - 5 god damns
s08e16 - Harvey - 11 god damns
s09e01 - Everything's Changed - 2 god damns
s09e02 - Special Master - 3 god damns
s09e03 - Windmills - 3 god damns
s09e04 - Cairo - 8 god damns
s09e05 - If The Shoe Fits - 4 god damns
s09e06 - Whatever It Takes - 9 god damns
s09e07 - Scenic Route - 6 god damns
s09e08 - Prisoner's Dilemma - 3 god damns
s09e09 - Thunder Away - 5 god damns
s09e10 - One Last Con - 13 god damns
Top 10 God Damn episodes:
1) s05e13 - God's Green Earth - 17 god damns
2) s04e06 - Litt the Hell Up - 16 god damns
3) s06e08 - Borrowed Time - 15 god damns
4) s05e16 - 25th Hour - 13 god damns
5) s06e03 - Back on the Map - 13 god damns
6) s09e10 - One Last Con - 13 god damns
7) s05e12 - Live to Fight - 12 god damns
8) s04e09 - Gone - 11 god damns
9) s05e14 - Self Defense - 11 god damns
10) s06e02 - Accounts Payable - 11 god damns
On avg 5.254 god damns per episode
```
