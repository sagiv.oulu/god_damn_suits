import re
import logging
import os

from bs4 import BeautifulSoup
from multiprocessing.dummy import Pool

import requests


WEBSITE = 'https://www.springfieldspringfield.co.uk'
THREADS = 300
LOG_LEVEL = os.getenv('LOG_LEVEL', 'INFO')

logging.basicConfig(level=getattr(logging, LOG_LEVEL))

def get_episode_links(tv_show: str):
    logging.debug(f'Getting links for all episodes of {tv_show}')
    main_page = requests.get(f'{WEBSITE}/episode_scripts.php?tv-show=suits').content
    main_page = BeautifulSoup(main_page, 'html.parser')
    links = main_page.select(".season-episode-title")
    formatted_links = [f"{WEBSITE}/{link.get('href')}" for link in links], [re.sub(r'^\d+\.\s', '', link.string) for link in links]
    logging.debug(f'Found {len(formatted_links)} links for episodes of {tv_show}')
    return formatted_links


def get_episode_script(link: str):
    logging.debug(f'Getting the script for episode: {link}')
    page = requests.get(link).content
    page = BeautifulSoup(page, 'html.parser')
    script = page.select('.scrolling-script-container')
    if len(script) != 1:
        raise AssertionError(f'Found {len(script)} scripts from the episode link: {link}')
    script = script[0]
    final_script = script.get_text().strip()

    logging.debug(f'Successfully got the script for episode: {link}')
    return final_script


def get_link_episode(link: str):
    episode = re.findall(r'^.*episode=(s\d+e\d+)$', link)
    if len(episode) != 1:
        raise ValueError(f'Invalid link format: {link}')
    episode = episode[0]
    return episode


def get_all_episodes(tv_show: str):
    links, titles = get_episode_links(tv_show=tv_show)
    link_title = zip(links, titles)
    episodes = list(map(lambda l: {'link': l[0], 'title': l[1], 'episode': get_link_episode(l[0])}, link_title))
    pool = Pool(processes=THREADS)
    episodes = pool.map(lambda episode: {**episode, 'script': get_episode_script(episode.get('link'))}, episodes)
    return episodes


if __name__ == '__main__':
    episodes = get_all_episodes('suits')
    episodes = list(map(lambda episode: {
        **episode,
        'god_damns': len(re.findall('(god[^a-z0-9]*damn)', episode['script'], re.IGNORECASE))
    }, episodes))
    for episode in episodes:
        print(f"{episode['episode']} - {episode['title']} - {episode['god_damns']} god damns")

    top_episodes = 10
    print(f'Top {top_episodes} God Damn episodes:')
    top_god_damn_episodes = list(sorted(episodes, key=lambda episode: episode['god_damns'], reverse=True))[:top_episodes]
    for index, episode in enumerate(top_god_damn_episodes):
        print(f"{index + 1}) {episode['episode']} - {episode['title']} - {episode['god_damns']} god damns")
    avg_god_damns = sum([episode['god_damns'] for episode in episodes]) / len(episodes)
    print(f'On avg {round(avg_god_damns, ndigits=3)} god damns per episode')
